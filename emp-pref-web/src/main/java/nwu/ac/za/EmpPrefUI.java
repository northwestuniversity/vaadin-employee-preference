package nwu.ac.za;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;

import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.menuapplication.InitialView;
import nwu.ac.za.framework.menuapplication.MenuApplicationUI;
import nwu.ac.za.framework.utility.PropertyValueRetriever;
import nwu.ac.za.util.EmployeePreferenceConstants;

@SuppressWarnings("serial")
public class EmpPrefUI extends MenuApplicationUI {

    private InitialView blankView;
    //	private ServiceBonusView serviceBonus;
//	private PensionFundView pensionFund;
    private ServiceBonusMenuView serviceBonusView;
    private PensionFundMenuView pensionFundView;
    // private ModuleMenuView moduleView;
    // private CurriculumMenuView curriculumView;
    // private ModuleMenuView shortCourseModuleView;
    // private CurriculumMenuView shortCourseCurriculumView;
    // protected AcademicOEUtility utility;
    // protected CopyOnWriteArrayList<OEData> orgData;
    protected boolean finishedLoadingOrgData = false;

    protected String appRuntimeEnvironment;
    private PropertyValueRetriever propertyValueRetriever = new PropertyValueRetriever();
    protected String hrServicePassword;
    protected String hrServiceUsername;

    // Constructor class
    public EmpPrefUI() {
        super();
        this.setBrowserTabTitle(EmployeePreferenceMessages.getString(EmployeePreferenceConstants.MessageCatalogue.APP_NAME));
        this.setMenuTitle(EmployeePreferenceMessages.getString(EmployeePreferenceConstants.MessageCatalogue.APP_NAME));
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        super.init(vaadinRequest);
        blankView = new InitialView(null);
        // serviceBonus = new ServiceBonusView("THis is the service bonus view
        // which is not yet implemented");
        // pensionFund = new PensionFundView("THis is the pension fund view
        // which is not yet implemented");
        serviceBonusView = new ServiceBonusMenuView(this);
        pensionFundView = new PensionFundMenuView(this);
        try {
            appRuntimeEnvironment = propertyValueRetriever.getPropertyValues("runEnvironment", "config.properties");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        menuInit(vaadinRequest);
    }

    //Method to create the navigator view
    public void createNavigatorViews() {
        // this.addNavigator("serviceBonus", serviceBonus);
        // this.addNavigator("pensionFund", pensionFund);
        this.addNavigator("serviceBonusView", serviceBonusView);
        this.addNavigator("pensionFundView", pensionFundView);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = EmpPrefUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }

    @Override
    public void createMenuItems() {
        // this.addMenuItem("serviceBonus", "Service Bonus");
        // this.addMenuItem("pensionFund", "Pension Fund not implemented");
        this.addMenuItem("serviceBonusView", EmployeePreferenceMessages.getString("EmployeePreferenceView.menuTitleServiceBonus"));
        this.addMenuItem("pensionFundView", EmployeePreferenceMessages.getString("EmployeePreferenceView.menuTitlePensionFund"));
    }

    @Override
    public void afterInit() {
        setDefaultNavigationView("serviceBonusView");
        setDefaultErrorView(blankView);

    }

    @Override
    public void beforeNavigateTo(String key) {
        System.out.println("EmpPrefUI.beforeNavigateTo for " + key);
    }

    @Override
    public void afterNavigateTo(String key) {
        System.out.println("EmpPrefUI.afterNavigateTo for " + key);
    }

    public String getHRServiceRegistryEnvironmentTypeKey(String serviceName, String version) {

        String serviceLookupKey;

        String[] output = version.split("\\.");
        String majorVersion = "V" + output[0];

        serviceLookupKey = "/" + appRuntimeEnvironment + "/" + serviceName + "/" + majorVersion;
        System.out.println("Service lookup key" + serviceLookupKey.toUpperCase());
        return serviceLookupKey.toUpperCase();
    }
}