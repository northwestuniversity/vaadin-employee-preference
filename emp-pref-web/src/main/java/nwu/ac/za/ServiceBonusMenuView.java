package nwu.ac.za;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.person.service.PersonService;
import ac.za.nwu.core.person.service.factory.PersonServiceClientFactory;
import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import client.employeeinformation.BonusProvisioningInformation;
import client.employeeinformation.EmployeeInformation;
import client.employeeinformation.EmployeeRequest;
import client.employeeinformation.HRCOILookupInformation;
import client.employeeinformation.HRLookupInformationRequest;
import client.employeeinformation.SetBonusProvisioningRequest;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.menuapplication.MenuApplicationUI;
import nwu.ac.za.framework.menuapplication.MenuApplicationView;
import nwu.ac.za.framework.utility.PropertyValueRetriever;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException.SeverityType;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException.Type;
import nwu.ac.za.util.EmployeePreferenceConstants;
import nwu.ac.za.view.impl.ServiceBonusAfrViewImpl;
import nwu.ac.za.view.impl.ServiceBonusEngViewImpl;
import nwu.ac.za.view.infc.ServiceBonusInfc;

public class ServiceBonusMenuView extends MenuApplicationView {

    private EmployeeInformation hrEmployeeInformationServie;
    private PropertyValueRetriever propertyValueRetriever = new PropertyValueRetriever();
    private String wsIAPIReadUser = null;
    private String wsIAPIReadUserPassword = null;
    private PersonService personService;
    private PersonBiographicInfo personBiographicInfo;
    public static final String WS_IAPI_READ_USER_NAME = "ws_iapiapp_read_username";
    public static final String WS_IAPI_READ_USER_NAME_PASSWORD = "ws_iapiapp_read_username_password";
    public static final String HR_EMPLOYEE_INFORMATION_SERVICE_VERSION = "hr-employeeinformation-service-version";

    private Boolean firstRun = true;
    private String lookupUser;

    private ServiceBonusInfc contentViewDetail;
    private String submitError;
    private boolean allowSubmit;
    public boolean isEmployee;
    private String currentOption;
    private String authenticatedUser;
    private boolean declarationChecked;
    private String taxDeductionOption;

    public ServiceBonusMenuView(MenuApplicationUI handleToMenuApplicationUI) {
        super(handleToMenuApplicationUI);
    }

    private static final long serialVersionUID = 1L;
    private static final String PROPERTY_NAME_IDENTITY_API_VERSION = "identity.api.version";

    private static final String SYSTEM_LANG_AF = "af";

    @Override
    public void enter(ViewChangeEvent event) {
        menuViewInitialization();

    }

    @Override
    public void afterUIInitialization() {
        this.lookupUser = this.handleToMenuApplicationUI.getAuthenticatedUser();
        setClosingDate();
        loadInitialData();
        mapDTOtoUIComponents();
        // registerSinglePageEvents();
        // refreshData();

    }

    @Override
    public String getBrowserTabTitle() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void setDetailView() {
        String lang = UI.getCurrent().getLocale().getLanguage();
        VerticalLayout view = null;

        try {
            if (lang.equals("af")) {
                view = new ServiceBonusAfrViewImpl();
            } else {
                view = new ServiceBonusEngViewImpl();
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        this.detailContent = view;
        contentViewDetail = (ServiceBonusInfc) this.detailContent;

    }

    @Override
    protected void serviceInitialization() {
        String hrUsername;
        try {
            hrUsername = propertyValueRetriever.getPropertyValues("username", "config.properties");

            String hrEncryptedPassword = propertyValueRetriever.getPropertyValues("password", "config.properties");
            String hrPassword = EncryptorUtility.decrypt(EmployeePreferenceConstants.SECKEY, EmployeePreferenceConstants.INITVECTOR, hrEncryptedPassword);
            String version = propertyValueRetriever.getPropertyValues(HR_EMPLOYEE_INFORMATION_SERVICE_VERSION, "config.properties");

            String hremployeelookupkey = ((EmpPrefUI) this.handleToMenuApplicationUI).getHRServiceRegistryEnvironmentTypeKey(HREmployeeInformationServiceClientFactory.EMPLOYEEINFORMATION_SERVICE, version);
            hrEmployeeInformationServie = HREmployeeInformationServiceClientFactory.getEmployeeInformationService(hremployeelookupkey, hrUsername, hrPassword);
        } catch (IOException | DoesNotExistException | MissingParameterException | PermissionDeniedException | OperationFailedException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }
        // Person Service Lookup
        try {
            readApplicationPropertyFile();
            String appRuntimeEnv = ((EmpPrefUI) this.handleToMenuApplicationUI).appRuntimeEnvironment;
            String personServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(PersonServiceClientFactory.PERSONSERVICE, PROPERTY_NAME_IDENTITY_API_VERSION, null, "config.properties", appRuntimeEnv);
            personService = PersonServiceClientFactory.getPersonService(personServiceLookupKey, this.wsIAPIReadUser, this.wsIAPIReadUserPassword);
        } catch (Exception e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }
    }

    @Override
    public void registerSinglePageEvents() {

        contentViewDetail.getCheckBoxConfirm().addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent e) {
                if (contentViewDetail.getCheckBoxConfirm().getValue()) {
                    declarationChecked = true;
                } else {
                    declarationChecked = false;
                }
            }
        });

        contentViewDetail.getSelectionTaxDeduction().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent e) {
                Property property = e.getProperty();
                Object propertyValue = property.getValue();
                String monthlyOption = EmployeePreferenceMessages.getString("ServiceBonusMenuView.Monthly");
                String annuallyOption = EmployeePreferenceMessages.getString("ServiceBonusMenuView.Annually");
                if (propertyValue != null && propertyValue.equals(monthlyOption)) {
                    taxDeductionOption = "Y"; // Monthly
                }
                if (propertyValue != null && propertyValue.equals(annuallyOption)) {
                    taxDeductionOption = "N"; // Annually
                }
            }
        });

        this.contentViewDetail.getOutputUniversityNumber().addValueChangeListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Property univNumberTextField = event.getProperty();
                String value = (String) univNumberTextField.getValue();
                if (value != null) {
                    resetUI();
                    lookupUser = value;
                    isEmployee = false;
                    loadInitialData();
                    mapDTOtoUIComponents();
                }
            }
        });

    }

    @Override
    public void beforeSaveValidations() {

        if (allowSubmit) {

            if (!declarationChecked) {
                new UserError(EmployeePreferenceMessages.getString("ServiceBonusMenuView.mandatory2"));

                String errorMsg = EmployeePreferenceMessages.getString("ServiceBonusMenuView.mandatory2");
                //+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere");
                //new Notification(errorMsg, "", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());
                throw new VaadinUIException(errorMsg, SeverityType.ERROR, Type.CLIENTSIDE, null);
                // return; // Display first error message only
            }
            if (taxDeductionOption == null) {
                //((AbstractComponent) contentViewDetail).setComponentError(
                new UserError(EmployeePreferenceMessages.getString("ServiceBonusMenuView.mandatory1"));
                String errorMsg = EmployeePreferenceMessages.getString("ServiceBonusMenuView.mandatory1");

                throw new VaadinUIException(errorMsg, SeverityType.ERROR, Type.CLIENTSIDE, null);
//								+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere"),
//						"", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());

            }
        } else {
            new Notification(submitError, "", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());
        }

    }

    @Override
    public void afterSave() {
        // TODO Auto-generated method stub
    }

    @Override
    public void beforeSave() {
    }

    @Override
    public void save() {

        if (taxDeductionOption != null && declarationChecked) {

            // setHRServiceSoapHeader();
            SetBonusProvisioningRequest sbpr = new SetBonusProvisioningRequest();
            sbpr.setEmployeeNumber(lookupUser);
            sbpr.setOption(taxDeductionOption);
            sbpr.setAuditUser(lookupUser);
            try {
                BonusProvisioningInformation bpi = hrEmployeeInformationServie.setBonusProvisioning(sbpr);
                if (bpi.getErrorMessage() != null) {
                    throw new VaadinUIException(bpi.getErrorMessage().getValue(), SeverityType.WARNING, Type.UNKNOWN);
//					new Notification(bpi.getErrorMessage().getValue()+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere"),
//							"", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());
                } else {

                    setCurrentSelectionBasedonHR();
                    String successfulMsg = EmployeePreferenceMessages.getString("ServiceBonusMenuView.successSave");
                    throw new VaadinUIException(successfulMsg, SeverityType.SUCCESS, Type.UNKNOWN);
                    //String successMsg = EmployeePreferenceMessages.getString("ServiceBonusMenuView.successSave");

                    //throw new VaadinUIException(successMsg, SeverityType.SUCCESS, Type.UNKNOWN);
                    //throw new VaadinUIException(errorMsg, SeverityType.ERROR, Type.CLIENTSIDE, null);

//					new Notification(
//							successfulMsg
//									+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere"),
//							"", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());


                }


            } catch (Exception ex) {
                Logger.getLogger(ServiceBonusMenuView.class.getName()).log(Level.SEVERE, null, ex);
                if (ex instanceof VaadinUIException) {
                    throw ex;
                }
                throw new VaadinUIException(ex, SeverityType.ERROR);
            }
        }
    }

    @Override
    public void loadInitialData() {

        System.out.println("load initial data..." + this.lookupUser);
        String systemLanguageTypeKey = "vss.code.LANGUAGE.3";


        if (this.getSession().getLocale().getLanguage().equals(SYSTEM_LANG_AF)) {
            systemLanguageTypeKey = "vss.code.LANGUAGE.2";
        }

        List<PersonAffiliationInfo> personAffiliations = null;
        try {
            personAffiliations = personService.getPersonAffiliation(this.lookupUser, EmployeePreferenceConstants.hrEmployeeContextInfo);
        } catch (DoesNotExistException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        } catch (InvalidParameterException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        } catch (MissingParameterException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        } catch (OperationFailedException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        } catch (PermissionDeniedException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        }

        for (PersonAffiliationInfo personAffiliation : personAffiliations) {
            if (personAffiliation.getAffiliationTypeKey()
                    .equals(EmployeePreferenceMessages.getString("ServiceBonusMenuView.EmployeeTypeKey"))) {
                isEmployee = true;
                break;
            }
            ;
        }

        if (isEmployee) {

        } else {

            submitError = EmployeePreferenceMessages.getString("ServiceBonusMenuView.onlyEmployees");
            //		+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere");
            disableUserInput();
            throw new VaadinUIException(submitError, SeverityType.ERROR, Type.CLIENTSIDE, null);
            //new Notification(submitError, "", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());
        }

        try {
            personBiographicInfo = personService.getPersonBiographicByLang(this.lookupUser, systemLanguageTypeKey,
                    EmployeePreferenceConstants.hrEmployeeContextInfo);
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException
                | OperationFailedException | PermissionDeniedException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        }

        allowUserSubmission();

    }

    private void setClosingDate() {

        HRLookupInformationRequest lookupCriteria = new HRLookupInformationRequest();
        lookupCriteria.setLookupType("NWU_BON_PROV_DEADLINE");
        lookupCriteria.setLanguage(EmployeePreferenceMessages.getString("ServiceBonusMenuView.language"));

        HRCOILookupInformation preferenceClosingDate = hrEmployeeInformationServie.getHRCodeLookups(lookupCriteria);
        String actualClosingDate = preferenceClosingDate.getLookupRecord().get(0).getLookupCode();

        this.contentViewDetail.getOutputClosingDate().setValue(actualClosingDate);
    }

    @Override
    public void refreshData() {
        // TODO Auto-generated method stub

    }

    @Override
    public void populateUIComboComponents() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resetUI() {
        this.contentViewDetail.getOutputUniversityNumber().setComponentError(null);
        this.contentViewDetail.getOutputUniversityNumber().setValidationVisible(false);

    }

    @Override
    public void mapDTOtoUIComponents() {
        firstRun = true;
        contentViewDetail.getOutputUniversityNumber().setValue(lookupUser);
        // contentViewDetail.getOutputUniversityNumberLabel().setValue(lookupUser);
        String titleKey = personBiographicInfo.getTitleTypeKey();
        String title = titleKey.substring(titleKey.lastIndexOf('.') + 1);
        String initials = personBiographicInfo.getInitials();
        String surname = personBiographicInfo.getLastName();
        contentViewDetail.getOutputTitleInitialsSurname().setValue(title + " " + initials + " " + surname);
        setReadOnlyFields();    // TEP-47
    }

    // TEP-47
    public void setReadOnlyFields() {
        try {
            if (propertyValueRetriever.getPropertyValues(EmployeePreferenceConstants.ALLOW_UNIV_NUMBER_INPUT, EmployeePreferenceConstants.PROPERTY_FILE_NAME).equals("false")) {
                this.contentViewDetail.getOutputUniversityNumber().setReadOnly(true);
                this.contentViewDetail.getOutputUniversityNumber().setVisible(true);    // TEP-49
                this.contentViewDetail.getOutputTitleInitialsSurname().setVisible(true);
            }
        } catch (IOException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        }
    }

    @Override
    public void mapUIComponentsToDTO() {
        // TODO Auto-generated method stub

    }

    // TODO NINA this must move to super class
    @Override
    public String getServiceRegistryEnvironmentTypeKey(String serviceName) {
        String DB = null;
        String serviceLookupKey;
        String appRuntimeEnv = ((EmpPrefUI) this.handleToMenuApplicationUI).appRuntimeEnvironment;
        if (appRuntimeEnv.equals("test")) {
            DB = "/V_TEST";
        } else if (appRuntimeEnv.equals("qa")) {
            DB = "/V_TEST";
        }
        // if (database != null) {
        // DB = "/" + database;
        // }

        if (DB != null) {
            serviceLookupKey = "/" + appRuntimeEnv + "/" + serviceName + "/" + "V4" + DB;
        } else {
            serviceLookupKey = "/" + appRuntimeEnv + "/" + serviceName + "/" + "V4";
        }
        System.out.println("Service lookup key" + serviceLookupKey.toUpperCase());
        return serviceLookupKey.toUpperCase();
    }

    public void readApplicationPropertyFile() {

        try {
            wsIAPIReadUser = propertyValueRetriever.getPropertyValues(WS_IAPI_READ_USER_NAME, "config.properties");
            String encyptedPassword = propertyValueRetriever.getPropertyValues(WS_IAPI_READ_USER_NAME_PASSWORD,
                    "config.properties");
            wsIAPIReadUserPassword = EncryptorUtility.decrypt(EmployeePreferenceConstants.SECKEY, EmployeePreferenceConstants.INITVECTOR, encyptedPassword);

        } catch (IOException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, VaadinUIException.Type.PROPERTYFILE, false);
        }

    }

    public void disableUserInput() {
        // selectionPercentages = contentViewDetail.getSelectionPercentages();
        // checkBoxConfirm = contentViewDetail.getCheckBoxConfirm();
        contentViewDetail.getSelectionTaxDeduction().setEnabled(false);
        contentViewDetail.getCheckBoxConfirm().setEnabled(false);
        // selectionPercentages.setEnabled(false);
        // checkBoxConfirm.setEnabled(false);
    }

    private BonusProvisioningInformation setCurrentSelectionBasedonHR() {
        EmployeeRequest bonusCriteria = new EmployeeRequest();
        bonusCriteria.setEmployeeNumber(this.lookupUser);
        BonusProvisioningInformation employeeServiceBonusInfo = hrEmployeeInformationServie
                .getBonusProvisioning(bonusCriteria);
        currentOption = employeeServiceBonusInfo.getProvisioningFlag().equals("Y")
                ? EmployeePreferenceMessages.getString("ServiceBonusMenuView.Monthly")
                : EmployeePreferenceMessages.getString("ServiceBonusMenuView.Annually");
        String currentOptionMSG = EmployeePreferenceMessages.getString("ServiceBonusMenuView.currentOption");
        contentViewDetail.getCurrentDeductionLabel().setValue(currentOption);
        contentViewDetail.getSelectionTaxDeduction().setValue(currentOption);

        // contentViewDetail.getSelectionTaxDeduction().setValue(currentOptionMSG
        // + "<u>" + currentOption + "</u>.");
        return employeeServiceBonusInfo;
    }

    public void allowUserSubmission() {
        try {
            BonusProvisioningInformation bpi = setCurrentSelectionBasedonHR();

            if (bpi.getErrorMessage() != null) {
                submitError = bpi.getErrorMessage().getValue();

                disableUserInput();
                throw new VaadinUIException(submitError, SeverityType.ERROR, Type.CLIENTSIDE, null);
//				new Notification(
//						bpi.getErrorMessage().getValue()
//								+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere"),
//						"", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());
            } else if (bpi.getDeadlinePassed().equals("N") && bpi.getEligibilityFlag().equals("Y")) {
                allowSubmit = true;
            } else {
                allowSubmit = false;

                disableUserInput();

                if (bpi.getDeadlinePassed().equals("Y")) {
                    submitError = EmployeePreferenceMessages.getString("ServiceBonusMenuView.passDeadlineDate");
                    //+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere");
                    throw new VaadinUIException(submitError, SeverityType.ERROR, Type.CLIENTSIDE, null);
                } else {
                    submitError = EmployeePreferenceMessages.getString("ServiceBonusMenuView.notEligible");
                    //+ EmployeePreferenceMessages.getString("ServiceBonusMenuView.clickHere");
                    throw new VaadinUIException(submitError, SeverityType.ERROR, Type.CLIENTSIDE, null);
                }

                //new Notification(submitError, "", Notification.TYPE_ERROR_MESSAGE, true).show(Page.getCurrent());

            }

        } catch (Exception ex) {
            submitError = ex.getMessage();
            Logger.getLogger(ServiceBonusMenuView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}