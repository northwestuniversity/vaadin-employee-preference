package nwu.ac.za.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import assemble.edu.common.dto.ContextInfo;

/**
 * Created by...
 */
public class EmployeePreferenceConstants {

    //Property file values
    public static final String PROPERTY_FILE_NAME = "config.properties";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String ALLOW_UNIV_NUMBER_INPUT = "allowUnivNumberInput";
    public static final String RUN_ENVIRONMENT = "runEnvironment";

    //Language constants
    public static final String LANGUAGE_AF = "af";
    public static final String MESSAGE_CATALOGUE_AF_NAME = "_af_ZA";
    public static final String MESSAGE_CATALOGUE_ENG_NAME = "_en_ZA";

    //Label Text
    public static final int PHASING_OUT_DATE_MONTH = 12; // December
    public static final int PHASING_OUT_DATE_DAY = 31;

    //Effective Date
    public static final Date EFFECTIVE_DATE = new GregorianCalendar(
            Calendar.getInstance().get(Calendar.YEAR) + 1, 0, 1).getTime();

    //Service Constants
    //StudentAcademicRegistrationByModuleOffering
    public static final String ACADEMIC_PERIOD_TYPEKEY = "vss.code.AcademicPeriod.YEAR";

    public static class MessageCatalogue {
        //Application Name
        public static final String APP_NAME = "EmployeePreferenceView.appName";
        //Component Text
        //Table Column Names - Shared
        public static final String CAMPUS_SCHOOL_2017_COLUMN_NAME = "EmployeePreferenceView.campusSchool2017";
        public static final String CAMPUS_SCHOOL_2018_COLUMN_NAME = "EmployeePreferenceView.campusSchool2018";
        public static final String USER_NOTES = "EmployeePreferenceView.userNotes";
        //Table Column Names - Module
        //public static final String MODULE_CODE_COLUMN_NAME = "EmployeePreferenceView.moduleCode";
        //public static final String MODULE_DESCRIPTION_COLUMN_NAME = "EmployeePreferenceView.moduleDescription";
        public static final String PHASING_OUT_DATE_COLUMN_NAME = "EmployeePreferenceView.phasingOutDate";
        public static final String STATUS_COLUMN_NAME = "EmployeePreferenceView.status";
        public static final String ERROR_STATUS_COLUMN_NAME = "EmployeePreferenceView.errorStatus";
        public static final String ERROR_MESSAGE_COLUMN_NAME = "EmployeePreferenceView.errorMessage";
        //Table Column Names - Curriculum
        public static final String QUALIFICATION_PROGRAM_COLUMN_NAME = "EmployeePreferenceView.qualificationProgram";
        public static final String DESCRIPTION_COLUMN_NAME = "EmployeePreferenceView.description";
        public static final String CURRICULUM_CODE_COLUMN_NAME = "EmployeePreferenceView.curriculumCode";
        public static final String CURRICULUM_DESCRIPTION_COLUMN_NAME = "EmployeePreferenceView.curriculumDescription";
        public static final String PHASING_OUT_DATE_LEVEL_1 = "EmployeePreferenceView.phasingOutDate.level1";
        public static final String PHASING_OUT_DATE_LEVEL_2 = "EmployeePreferenceView.phasingOutDate.level2";
        public static final String PHASING_OUT_DATE_LEVEL_3 = "EmployeePreferenceView.phasingOutDate.level3";
        public static final String PHASING_OUT_DATE_LEVEL_4 = "EmployeePreferenceView.phasingOutDate.level4";
        public static final String PHASING_OUT_DATE_LEVEL_5 = "EmployeePreferenceView.phasingOutDate.level5";

        //Notification Text
        //Save
        public static final String SAVE_SUCCESSFUL = "EmployeePreferenceView.saveSuccess";
        public static final String SAVE_ERROR = "EmployeePreferenceView.saveError";
        //Invisible Columns
        //Module
        public static Object[] INVISIBLE_COLUMNS_MODULE = new Object[]{STATUS_COLUMN_NAME, ERROR_STATUS_COLUMN_NAME,
                ERROR_MESSAGE_COLUMN_NAME};
        //Curriculum
        public static Object[] INVISIBLE_COLUMNS_CURRICULUM = new Object[]{};

        //Tooltip Text
        public static final String FIELD_DISABLED = "EmployeePreferenceView.fieldDisabled";
        public static final String FIELD_DISABLED_REASON = "EmployeePreferenceView.fieldDisabledReason";

        public static final String EMPLOYEEERROR = "EmployeePreferenceView.employeeError";
        public static final String EMPLOYEEERRORVALUE = "EmployeePreferenceView.employeeErrorValue";

        public static final String OE_ERROR = "EmployeePreferenceView.oeError";
    }

    // define security keys and initVector
    public static final String SECKEY = "Bar12345Bar12345";
    public static final String INITVECTOR = "RandomInitVector";
    private static final String HR_EMPLOYEE_SUBSCRIBER = "EMPLOYEEPREFERENCE";
    public static ContextInfo hrEmployeeContextInfo = new ContextInfo(HR_EMPLOYEE_SUBSCRIBER);
}
