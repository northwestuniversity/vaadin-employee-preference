package nwu.ac.za;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class EmployeePreferenceMessages {
	private static final String BUNDLE_NAME = "messages/EmployeePreferenceMessages"; //$NON-NLS-1$

	private EmployeePreferenceMessages() {
	}

	public static String getString(String key) {
		try {
			//System.out.println("Default = " + Locale.getDefault());
			ResourceBundle myResourceBundle = ResourceBundle.getBundle(BUNDLE_NAME + "_" + Locale.getDefault());

			Locale.getDefault();
			String msg = myResourceBundle.getString(key);
			
			if (msg == null) {
				return "MSG Catalog does not exist for key : " + key;
			} 
			return msg;
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	public static String getString(String key, String fileName) {
		try {
			//System.out.println("Default = " + Locale.getDefault());
			ResourceBundle myResourceBundle = ResourceBundle.getBundle(BUNDLE_NAME + fileName);
			Locale.getDefault();
			return myResourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
