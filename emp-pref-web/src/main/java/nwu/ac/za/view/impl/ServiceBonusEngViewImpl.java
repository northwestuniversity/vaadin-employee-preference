package nwu.ac.za.view.impl;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;

import nwu.ac.za.ui.vaadin.ServiceBonus;
import nwu.ac.za.view.infc.ServiceBonusInfc;

public class ServiceBonusEngViewImpl extends ServiceBonus implements ServiceBonusInfc{
	
	@Override
	public Label getUnivNumber() {
		return this.univNumber;
	}

	@Override
	public TextField getOutputUniversityNumber() {
		return this.outputUniversityNumber;
	}

	@Override
	public Label getOutputTitleInitialsSurname() {
		return this.outputTitleInitialsSurname;
	}
	
	@Override
	public CheckBox getCheckBoxConfirm() {
		return this.checkBoxConfirm;
	}
	
	@Override
	public Label getOutputClosingDate() {
		return this.outputClosingDate;
	}
	
	@Override
	public OptionGroup getSelectionTaxDeduction() {
		return this.selectionTaxDeduction;
	}

	@Override
	public Label getCurrentDeductionLabel() {
		return this.currentDeductionLabel;
	}


}
