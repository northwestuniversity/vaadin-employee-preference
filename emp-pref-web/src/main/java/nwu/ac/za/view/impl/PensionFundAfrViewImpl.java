package nwu.ac.za.view.impl;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import nwu.ac.za.ui.vaadin.PensionFundAfr;
import nwu.ac.za.view.infc.PensionFundInfc;

public class PensionFundAfrViewImpl extends PensionFundAfr implements PensionFundInfc{

	@Override
	public Label getUnivNumber() {
		return this.univNumber;
	}

	@Override
	public TextField getOutputUniversityNumber() {
		return this.outputUniversityNumber;
	}

	@Override
	public Label getOutputTitleInitialsSurname() {
		return this.outputTitleInitialsSurname;
	}
	@Override
	public Label getOutputClosingDate() {
		return this.outputClosingDate;
	}
	
	@Override
	public OptionGroup getSelectionPercentages() {
		return this.selectionPercentages;
	}
	
	@Override
	public CheckBox getCheckBoxConfirm() {
		return this.checkBoxConfirm;
	}
	
	@Override
	public VerticalLayout getVerticalLayout() {
		return this.verticalLayout;
	}

	@Override
	public Label getOutputPercentage() {
		return this.outputPercentage;
	}
}
