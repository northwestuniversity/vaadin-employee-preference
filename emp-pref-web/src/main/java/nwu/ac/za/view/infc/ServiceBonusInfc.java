package nwu.ac.za.view.infc;

import com.vaadin.ui.*;

public interface ServiceBonusInfc {
	public Label getUnivNumber();
	public TextField getOutputUniversityNumber();
	public Label getOutputTitleInitialsSurname();
	public CheckBox getCheckBoxConfirm();
	public Label getOutputClosingDate();
	public OptionGroup getSelectionTaxDeduction();
	public Label getCurrentDeductionLabel();
}
