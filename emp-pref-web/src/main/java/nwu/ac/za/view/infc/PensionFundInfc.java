package nwu.ac.za.view.infc;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public interface PensionFundInfc {
	public VerticalLayout getVerticalLayout();
	public Label getUnivNumber();
	public TextField getOutputUniversityNumber();
	public Label getOutputTitleInitialsSurname();
	public CheckBox getCheckBoxConfirm();
	public Label getOutputClosingDate();
	public OptionGroup getSelectionPercentages();
	public Label getOutputPercentage();
}